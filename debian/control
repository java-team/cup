Source: cup
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Torsten Werner <twerner@debian.org>
Section: java
Priority: optional
Build-Depends: ant,
               cup,
               debhelper-compat (= 13),
               default-jdk,
               jflex,
               maven-repo-helper
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/java-team/cup
Vcs-Git: https://salsa.debian.org/java-team/cup.git
Homepage: http://www2.cs.tum.edu/projects/cup/

Package: cup
Architecture: all
Depends: default-jre-headless | java8-runtime-headless,
         ${misc:Depends}
Suggests: java-compiler
Description: LALR parser generator for Java(tm)
 CUP is the "Constructor of Useful Parsers", a system for generating
 parsers from simple LALR specifications.  It serves the same role as the
 widely used program YACC and in fact offers most of the features of YACC.
 However, CUP is written in Java, uses specifications including embedded
 Java code, and produces parsers which are implemented in Java.
